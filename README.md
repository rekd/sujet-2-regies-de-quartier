# Sujet n°2 : Marchés publics et vie associative - Comité National de Liaison des Régies de Quartiers (https://www.regiedequartier.org/).

+ *Phase d'avancement actuelle :* lorem ipsum.
+ *Compétences associés :* lorem ipsum.

##### #0 | Résultats obtenus lors du hackathon "À l'asso des données"
Cette section contiendra des informations sur les résultats obtenus dans le cadre du hackathon "À l'asso des données".

##### #1 | Présentation du Comité National de Liaison des Régies de Quartiers
Lorem ipsum.

##### #2 | Problématique
Lorem ipsum.

##### #3 | Le défi proposé
Lorem ipsum.

##### #4 | Livrables
Lorem ipsum.

##### #5 | Ressources à disposition pour résoudre le défi
Lorem ipsum.

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de stand-up meeting ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
Lorem ipsum.